#!/usr/bin/env python3
# -*- coding: utf-8-unix -*-

"""
ControlM.py - version and date, see below

Author : Alexandre Norman - norman at xael.org
Licence : GPL v3 or any later version


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contributors :
 - Jean-Lou Schmidt
 - Olivier Singer

Documentation :
 - EM630100UT68548.pdf [CONTROL-M/Enterprise Manager - Utility Guide]
   p33 
 - MVS63010PR63689.pdf [CONTROL-M/Job Parameter and Variable Reference Guide]
   p205
"""

__author__ = 'Alexandre Norman (norman at xael.org)'
__version__ = '0.4.11b'
__last_modification__ = '2016.01.12'


import codecs
import subprocess
import sys
import xml.dom.minidom
import logging
import pprint
import tarfile
import re

try:
    import clize
except ImportError:
    print("This program uses clize. See : https://github.com/epsy/clize")
    sys.exit(1)




class ControlM:
    def __init__(self):
        """
        init class
        """
        self.__LOG__ = logging.getLogger("ControlM")
        self.__LOG__.debug("** ControlM::__init__")
        self.nodes = []
        self.warnings = []
        self.errors = []
        self.libmemsym_values = {}
        self.libmemsym_used = {}
        # http://ithowtoit.blogspot.fr/2012/11/control-m-system-variables.html
        self.libmemsym_builtin_values = {
            '%%$DATE': 'builtin',
            '%%$NEXT': 'builtin',
            '%%$ODATE': 'builtin',
            '%%$OYEAR': 'builtin',
            '%%$PREV': 'builtin',
            '%%$RDATE': 'builtin',
            '%%$RYEAR': 'builtin',
            '%%$YEAR': 'builtin',
            '%%APPLGROUP': 'builtin',
            '%%APPLIC': 'builtin',
            '%%AVG_CPU': 'builtin',
            '%%AVG_TIME': 'builtin',
            '%%BLANKn': 'builtin',
            '%%CENT': 'builtin',
            '%%COMPSTAT': 'builtin',
            '%%CYCLIC': 'builtin',
            '%%DATACENTER': 'builtin',
            '%%DATACENTER': 'builtin',
            '%%DATE': 'builtin',
            '%%DAY': 'builtin',
            '%%GROUP_ORDID': 'builtin',
            '%%JOBID': 'builtin',
            '%%JOBNAME': 'builtin',
            '%%JULDAY': 'builtin',
            '%%MEMLIB': 'builtin',
            '%%MONTH': 'builtin',
            '%%NEXT': 'builtin',
            '%%NODEID': 'builtin',
            '%%OCENT': 'builtin',
            '%%ODATE': 'builtin',
            '%%ODAY': 'builtin',
            '%%OJULDAY': 'builtin',
            '%%OMONTH': 'builtin',
            '%%ORDERID': 'builtin',
            '%%OWDAY': 'builtin',
            '%%OWNER': 'builtin',
            '%%OYEAR': 'builtin',
            '%%POSTCMD': 'builtin',
            '%%PRECMD': 'builtin',
            '%%PREV': 'builtin',
            '%%RDATE': 'builtin',
            '%%RDAY': 'builtin',
            '%%RJULDAY': 'builtin',
            '%%RMONTH': 'builtin',
            '%%RUNCOUNT': 'builtin',
            '%%RWDAY': 'builtin',
            '%%RYEAR': 'builtin',
            '%%SCHEDTAB': 'builtin',
            '%%SD_CPU': 'builtin',
            '%%SD_TIME': 'builtin',
            '%%TIME': 'builtin',
            '%%WDAY': 'builtin',
            '%%YEAR': 'builtin',
            }
        return

    def check_libmemsym(self, tar, libmemsym_filename):
        """
        Check variable definition in libmemsys
        """
        self.__LOG__.debug("** ControlM::check_libmemsys {0} - {1}".format(tar, libmemsym_filename))
        for t in tar.getnames():
            self.__LOG__.debug("tar : {0}".format(t))
            if libmemsym_filename in t:
                #libmemsym = tar.extractfile(t).read()
                # %%VCOUR_CMD=/appli/PORTAIL-DPIH/vcour/cmd
                for line in tar.extractfile(t).readlines():
                    line = (line.decode('utf-8'))
                    if '=' in line:
                        try:
                            var_nam, value = line.strip().split('=', 1)
                        except ValueError:
                            self.__LOG__.error('LibMemSym Error : {0}, {1}'.format(line, line.split()))
                            continue
                    elif line == '\n':
                        # empty line. don't care
                        continue
                    else:
                        self.__LOG__.error('LibMemSym Error2 : {0}, {1}'.format(line, line.split()))
                        continue

                    if var_nam in self.libmemsym_values:
                        self.__LOG__.warning("Redefinition of libmemsym variable {0} (new value: {2} / old value : {1})".format(var_nam, self.libmemsym_values[var_nam], value))
                        
                        self.warnings.append(
                            {
                                'job': '-',
                                'reason': "Red&eacute;finition d'une variable libmemsym {0} (nouvelle valeur : {2} / valeur actuelle : {1})".format(var_nam, self.libmemsym_values[var_nam], value),
                                'risk': 'Non ma&icirc;tris&eacute;.'
                                }
                            )

                    self.libmemsym_values[var_nam] = value
                    self.__LOG__.debug("libmemsym_values[{1}] : {0}".format(value, var_nam))
        return

    def read_xml(self, controlm):
        """
        Reads given filename, either an XML schedtable or a TAR file containing an XML schedtable

        Returns the chain description as a list
        """
        self.__LOG__.debug("** ControlM::read_xml")

        data = ''
        if controlm[-4:] == ".tar":
            tar = tarfile.open(controlm)
            found_file = False
            filename = ''
            for t in tar.getnames():
                if 'libmemsym.idx' in t or 'liste_lib.dat' in t:
                    libmemsym = tar.extractfile(t).read()
                    if not (isinstance(libmemsym, str)):
                        libmemsym_filename = str('{0}.lib'.format(str(libmemsym.decode('ascii')).split(':')[0]))
                    else:
                        libmemsym_filename = str('{0}.lib'.format(str(libmemsym).split(':')[0]))

                    self.__LOG__.debug("** ControlM::read_xml libmemsym_filename : {0}".format(libmemsym_filename))
                    self.check_libmemsym(tar, libmemsym_filename)
                    self.__LOG__.debug("libmemsym {1} : {0}".format(self.libmemsym_values, libmemsym_filename))

                if 'schedtable.xml' in t:
                    data = tar.extractfile(t).read()
                    filename = t
                    found_file = True
                elif "SCHEDTAB" in t:
                    data = tar.extractfile(t).read()
                    filename = t
                    found_file = True
                if 'nodegroup.lst' in t or 'NODEGROUP.dat' in t:
                    nodedata = tar.extractfile(t).read()
                    # DPHB00TAI1=pcyyi11p6
                    # DPHB00TAI2=pcyyi10p6
                    for line in nodedata.split():
                        try:
                            if '=' in str(line):
                                # v6.4
                                node = str(line).lower().split('=')[1].replace("'", "")
                            else:
                                # v8
                                node = str(line).lower().split(':')[1].replace("'", "")

                            self.nodes.append(node)
                            self.__LOG__.debug("Nodes : {0}".format(node))
                        except ValueError:
                            pass
                    
            if found_file == False:
                class ProblemWithFile_XML_not_found(Exception):
                    """
                    Class for raising exceptions
                    """                    
                    pass
                raise ProblemWithFile_XML_not_found('Problem extracting tar file (xml not found):\n{0}\n{1}'.format(controlm, tar.getnames()))

        else:
            if sys.version_info >= (3, 0):
                with open(controlm, mode='r', encoding='ISO-8859-1') as f:
                    data = f.read()
            else:
                with open(controlm, mode='r') as f:
                    data = f.read()


        chain = []
        date_last_upload = ''
        dom = xml.dom.minidom.parseString(data)

        # test duplicate jobs / abort if found
        fulljob_list = {}

        # V6-v8 Compatibility layer
        clv68 = {}
        if len(dom.getElementsByTagName('SCHED_TABLE'))>0:
            clv68['FOLDER'] = 'SCHED_TABLE'
            clv68['FOLDER_NAME'] = 'TABLE_NAME'
            clv68['GROUP'] = 'GROUP'

        elif len(dom.getElementsByTagName('FOLDER'))>0:
            clv68['FOLDER'] = 'FOLDER'
            clv68['FOLDER_NAME'] = 'FOLDER_NAME'
            clv68['GROUP'] = 'SUB_APPLICATION'
        else:
            raise ValueError('Unknown file format. Reading file {0}'.format(filename))

        for dsci in dom.getElementsByTagName(clv68['FOLDER']):
            chain_name = dsci.getAttributeNode(clv68['FOLDER_NAME']).value
            date_last_upload = dsci.getAttributeNode('LAST_UPLOAD').value
            jobs = {}

            for job in dsci.getElementsByTagName('JOB'):
                job_name = job.getAttributeNode('JOBNAME').value
                try:
                    fulljob_list[job_name] = fulljob_list[job_name]+1
                except KeyError:
                    fulljob_list[job_name] = 1

            errors = False
            for j in fulljob_list:
                if fulljob_list[j] > 1:
                    self.__LOG__.error("Duplicate Job name {0}. ABORTING.".format(j))
                    self.errors.append(
                        {
                            'job': job_name,
                            'reason': "Le nom de job est en doublon.",
                            'risk': "MAJEUR, cha&icirc;ne non conforme."
                            }
                        )
                    errors = False
            if errors:
                return [[(chain_name, date_last_upload), {}]]
                    

        nodeid_list = []

        for dsci in dom.getElementsByTagName(clv68['FOLDER']):
            chain_name = dsci.getAttributeNode(clv68['FOLDER_NAME']).value
            date_last_upload = dsci.getAttributeNode('LAST_UPLOAD').value
            jobs = {}

            for job in dsci.getElementsByTagName('JOB'):

                group = job.getAttributeNode(clv68['GROUP']).value
                job_name = job.getAttributeNode('JOBNAME').value

                if fulljob_list[job_name] > 1:
                    new_job_name = "{0}.dup_name_{1}".format(job_name, fulljob_list[job_name])
                    self.__LOG__.error("Duplicate job name {0} (group: {1}) : renamed {2} for analysis".format(job_name, group, new_job_name))

                    self.errors.append(
                        {
                            'job': job_name,
                            'reason': "Nom de job en doublon {0} (group: {1}) : renomm&eacute; {2} pour l'analyse.".format(job_name, group, new_job_name),
                            'risk': "Inconnu."
                            }
                        )

                    fulljob_list[job_name] -= 1 
                    job_name = new_job_name

                # input conditions
                inc = []
                for incond in job.getElementsByTagName('INCOND'):
                    data = {
                        'name': incond.getAttributeNode('NAME').value,
                        'cond': incond.getAttributeNode('AND_OR').value
                        }
                    try:
                        data['op'] = incond.getAttributeNode('OP').value
                    except:
                        data['op'] = ''

                    inc.append(data)
                    if len(incond.getAttributeNode('NAME').value) > 20:
                        self.__LOG__.error("Input condition name too long (max 20 char / {2}) [job: {1} / cond: {0}]".format(incond.getAttributeNode('NAME').value,
                                                                                                                             job_name,
                                                                                                                             len(incond.getAttributeNode('NAME').value)))
                        self.errors.append(
                            {
                                'job': job_name,
                                'reason': "Le nom de la condition d'entr&eacute;e <tt>{0}</tt> est trop long (maximum 20 caract&egrave;res, elle en fait {1}).".format(incond.getAttributeNode('NAME').value, len(incond.getAttributeNode('NAME').value)),
                                'risk': "La condition ne pourra pas &ecirc;tre d&eacute;planifi&eacute;e par les scripts standard."
                                }
                            )
                            

                outc = []
                # output conditions
                for outcond in job.getElementsByTagName('OUTCOND'):
                    # SIGN : Indicates whether the specified condition is to be added (created) or deleted.
                    # + Adds (creates) the prerequisite condition. Default.
                    # - Deletes the prerequisite condition
                    outc.append({'name': outcond.getAttributeNode('NAME').value,
                                 'sign': outcond.getAttributeNode('SIGN').value})

                    if len(outcond.getAttributeNode('NAME').value) > 20:
                        self.__LOG__.error("Output condition name too long (max 20 char / {2}) [job: {1} / cond: {0}]".format(outcond.getAttributeNode('NAME').value,
                                                                                                                              job_name,
                                                                                                                              len(outcond.getAttributeNode('NAME').value)))

                        self.errors.append(
                            {
                                'job': job_name,
                                'reason': "Le nom de la condition de sortie <tt>{0}</tt> est trop long (maximum 20 caract&egrave;res, elle en fait {1}).".format(outcond.getAttributeNode('NAME').value, len(outcond.getAttributeNode('NAME').value)),
                                'risk': "La condition ne pourra pas &ecirc;tre d&eacute;planifi&eacute;e par les scripts standard."
                                }
                            )
                    else:
                        cond = outcond.getAttributeNode('NAME').value
                        if len(cond) != 20 or cond[0:9] != chain_name or cond[9:9+8] != job_name or cond[-3:] not in ['-OK', '-KO', '-ER', '-C0', '-C1', '-C2', '-C3', '-C4', '-C5', '-C6', '-C7', '-C8', '-C9', '-LO']:
                            # conformity
                            self.__LOG__.warning("OUTPUT CONDITION NAME not compliant (Job:{0}, Input Condition:{1})".format(job_name, cond))
                       
                            reason = '<br><b>Raisons de l\'alerte</b> :<ul>'
                            if len(cond) != 20:
                                reason += '<li>Le nom de la condition doit &ecirc;tre compos&eacute; de 20 caract&egrave;res (ici : {0}).</li>'.format(len(cond))
                            
                            if cond[0:9] != chain_name:
                                reason += '<li>9 caract&egrave;res repr&eacute;sentant le nom du r&eacute;f&eacute;rentiel associ&eacute; (<tt>{0}</tt>)</li>'.format(chain_name)

                            if cond[9:9+8] != job_name[0:8]:
                                reason += '<li>8 caract&egrave;res repr&eacute;sentant le nom du job associ&eacute; (attendu : <tt>{0}</tt>)</li>'.format(job_name[0:8])

                            if cond[-3:] not in ['-OK', '-KO', '-ER', '-C0', '-C1', '-C2', '-C3', '-C4', '-C5', '-C6', '-C7', '-C8', '-C9', '-LO']:
                                reason += '<li>3 caract&egrave;res de terminaison (attendu : {0})</li>'.format(cond[-3:])

                            reason += '</ul>'

                            self.warnings.append(
                                {
                                    'job': job_name,
                                    'reason': "Nom de condition de sortie non conforme<br>OUTCOND: <tt>{0}</tt>".format(cond),
                                    'risk': '<b>Non respect de la norme</b><br>Le nom de la condition doit &ecirc;tre compos&eacute; de 20 caract&egrave;res se d&eacute;composant comme suit :<li>9 caract&egrave;res repr&eacute;sentant le nom du r&eacute;f&eacute;rentiel associ&eacute;</li><li>8 caract&egrave;res repr&eacute;sentant le nom du job associ&eacute;</li><li>3 caract&egrave;res de terminaison parmi -OK, -KO, -ER, -C0, -C1, -C2, -C3, -C4, -C5, -C6, -C7, -C8, -C9, -LO</li><ul><li>-OK : Terminaison correcte</li><li>-KO : Terminaison incorrecte</li><li>-ER : Condition non calcul&eacute;e par le script charge_cond.sh</li><li>-C0, C1...C9 : Condition de sortie d\'erreur</li><li>-LO : Terminaison des jobs "lock" (test de fichier flag)</li></ul></ul>' + reason
                                    }
                                )


                    ocn = outcond.getAttributeNode('NAME').value
                    optimal_ocn1 = "{0}{1}-OK".format(chain_name, job_name)
                    optimal_ocn2 = "{0}{1}-NOK".format(chain_name, job_name)
                    if (ocn not in [optimal_ocn1, optimal_ocn2]) and outcond.getAttributeNode('SIGN').value in ['ADD', '+']:
                        self.__LOG__.error("Output condition name not valid : {0} (chain : {2} / job : {1})".format(ocn, job_name, chain_name))

                        if ocn[:-3] == "-OK":
                            gocn = optimal_ocn1
                        elif ocn[:-4] == "-NOK":
                            gocn = optimal_ocn2
                        else:
                            gocn = optimal_ocn1

                        self.errors.append(
                            {
                                'job': job_name,
                                'reason': "La conformation du nom de la condition de sortie <tt>{0}</tt> est non valide. Devrait &ecirc;tre : <tt>{1}</tt>".format(ocn, gocn),
                                'risk': ''
                                }
                            )


                # on conditions

                ## <OUTCOND NAME="DPPH00P01NFINJSYS-OK" ODATE="ODAT" SIGN="ADD" />
                ## <ON
                ##  CODE="NOTOK"
                ##  >
                ##  <DOCOND NAME="DPPH00P01NFINJSYS-KO" ODATE="ODAT" SIGN="ADD" />
                ## </ON>
                ## <ON
                ##  CODE="NOTOK"
                ##  STMT="*"
                ##  >
                ##  <DOCOND NAME="DPPH00P01NSYNPOP1-OK" ODATE="ODAT" SIGN="ADD" />
                ## </ON>   
                ## --- v8 ---------------------------------------------------------
                ## <ON CODE="COMPSTAT EQ 1" STMT="*">
                ##     <DOACTION ACTION="OK"/>
                ##     <DOCOND NAME="DBPEINT01NFAND001-OK" ODATE="ODAT" SIGN="+"/>
                ## </ON>
                ## <ON CODE="COMPSTAT NE 1" STMT="*">
                ##     <DOACTION ACTION="NOTOK"/>
                ##     <DOCOND NAME="DBPEINT01NFAND001-KO" ODATE="ODAT" SIGN="+"/>
                ## </ON>
             
                onc = []
                for oncond in job.getElementsByTagName('ON'):
                    code = oncond.getAttributeNode('CODE').value
                    try:
                        stmt = oncond.getAttributeNode('STMT').value
                    except AttributeError:
                        stmt = ""


                    onc_local_docond = []
                    for docond in oncond.getElementsByTagName('DOCOND'):
                        docode = docond.getAttributeNode('NAME').value
                        dodate = docond.getAttributeNode('ODATE').value
                        dosign = docond.getAttributeNode('SIGN').value

                        onc_local_docond.append(
                            {
                                'docond': docond.nodeName,
                                'docode': docode,
                                'dodate': dodate,
                                'dosign': dosign
                                }
                            )

                        self.__LOG__.warning("Suspicious ON condition : {0} (chain:{2} / job:{1} / docode:{3} / dodate:{4} / dosign:{5})".format(code, job_name, chain_name, docode, dodate, dosign))
                        self.warnings.append(
                            {
                                'job': job_name,
                                'reason': 'Modification de la condition de sortie :<br>ON condition : <tt>{0} (docode:{2} / dodate:{3} / dosign:{4})</tt>'.format(code, chain_name, docode, dodate, dosign),
                                'risk': ''
                                }
                            )

                    onc_local_do = []
                    for do in oncond.getElementsByTagName('DO'):
                        doaction = do.getAttributeNode('ACTION').value

                        onc_local_do.append(
                            {
                                'do': do.nodeName,
                                'doaction': doaction
                                }
                            )

                        self.__LOG__.warning("Suspicious ON condition : {0} (chain:{1} / job:{2} / stmt:{3} / doaction:{4})".format(code, job_name, chain_name, stmt, doaction))
                        self.warnings.append(
                            {
                                'job': job_name,
                                'reason': 'Suspicious ON condition : <tt>{0}<br> (chain:{1} / jop:{2} / stmt:{3} / doaction:{4})</tt>'.format(code, job_name, chain_name, stmt, doaction),
                                'risk': ''
                                }
                            )

                    onc_local = {
                        'name': code,
                        'stmt': stmt,
                        'docond': onc_local_docond,
                        'do': onc_local_do
                        }

                    onc.append(onc_local)


                # job's values
                try:
                    task_type = job.getAttributeNode('TASKTYPE').value
                except AttributeError:
                    task_type = ""

                try:
                    owner = job.getAttributeNode('OWNER').value
                except AttributeError:
                    owner = ""
                try:
                    owner = job.getAttributeNode('RUN_AS').value
                except AttributeError:
                    owner = ""

                try:
                    confirm = job.getAttributeNode('CONFIRM').value
                except AttributeError:
                    confirm = '0'

                try:
                    job_time = job.getAttributeNode('TIMEFROM').value
                except AttributeError:
                    job_time = '-'
                try:
                    job_weekdays = job.getAttributeNode('WEEKDAYS').value
                except AttributeError:
                    job_weekdays = '-'

                try:
                    job_days = job.getAttributeNode('DAYS').value
                except AttributeError:
                    job_days = '-'

                try:
                    job_days_and_or = job.getAttributeNode('DAYS_AND_OR').value
                except AttributeError:
                    job_days_and_or = '-'


                try:
                    if sys.version_info >= (3, 0):
                        job_nodeid = job.getAttributeNode('NODEID').value
                    else:
                        job_nodeid = job.getAttributeNode('NODEID').value.encode('UTF-8')
                except AttributeError:
                    job_nodeid = '-'
                else:
                    if job_nodeid not in nodeid_list:
                        nodeid_list.append(job_nodeid)


                try:
                    if sys.version_info >= (3, 0):
                        desc = job.getAttributeNode('DESCRIPTION').value
                    else:
                        desc = job.getAttributeNode('DESCRIPTION').value.encode('UTF-8')
                except AttributeError:
                    desc = ''


                # test for inline scripts
                try:
                    use_inline_script = job.getAttributeNode('USE_INSTREAM_JCL').value
                    assert use_inline_script == 'Y'
                except:
                    use_inline_script = 'N'
                    inline_script = 'N/A'
                    try:
                        cmdline = job.getAttributeNode('CMDLINE').value
                    except AttributeError:
                        cmdline = ''
                else: # no exception raised
                    try:
                        inline_script = job.getAttributeNode('INSTREAM_JCL').value.replace('\n', '\\n')
                    except:
                        inline_script = 'N/A'
                        cmdline = 'N/A'
                    else:
                        cmdline = '::InlineScript::'





                try:
                    maxwait = job.getAttributeNode('MAXWAIT').value
                except AttributeError:
                    maxwait = ""



                # SAP stuff
                # APPL_FORM="SAP Business Warehouse"
                # APPL_FORM="SAP R3"
                # APPL_TYPE="SAP"
                # APPL_TYPE="OS"

                # APPL_FORM="File Watcher"
                # APPL_TYPE="FileWatch"
                try:
                    appl_form = job.getAttributeNode('APPL_FORM').value
                except AttributeError:
                    appl_form = ''

                try:
                    appl_type = job.getAttributeNode('APPL_TYPE').value
                except AttributeError:
                    appl_type = ''


                # test for filewatcher
                if appl_type == 'FileWatch':
                    cmdline = '::FileWatch::'




                try:
                    cyclic = job.getAttributeNode('CYCLIC').value
                except AttributeError:
                    cyclic = ''

                if cyclic == '1':
                    try:
                        cyclic_type = job.getAttributeNode('CYCLIC_TYPE').value
                    except AttributeError:
                        cyclic_type = ''
                    else:
                        if cyclic_type in ["SpecificTimes", "S"]:
                            try:
                                interval_tolerance = job.getAttributeNode('CYCLIC_TOLERANCE').value
                            except AttributeError:
                                interval_tolerance = 0
                            try:
                                interval = job.getAttributeNode('CYCLIC_TIMES_SEQUENCE').value
                            except AttributeError:
                                interval = ""
                            interval = '{0} (+/- {1})'.format(interval, interval_tolerance)
                        elif cyclic_type in ["Interval", "C"]:
                            try:
                                interval = job.getAttributeNode('INTERVAL').value
                            except AttributeError:
                                interval = ""
                        else:
                            self.__LOG__.error("Error unknown CYCLIC_TYPE : [job: {1} / cyclic: {0}] - please send tar to update the code".format(cyclic_type, job_name))
                            interval = ""
                            self.errors.append(
                                {
                                    'job': job_name,
                                    'reason': "Error unknown CYCLIC_TYPE : [{0}]".format(cyclic_type),
                                    'risk': ''
                                    }
                                )

                else:
                    cyclic_type = ""
                    interval = "-"

                months = ''
                for month in ['JAN', 'FEB', 'MAR', 'APR',  'MAY',  'JUN',  'JUL',  'AUG',  'SEP',  'OCT',  'NOV',  'DEC']:
                    try:
                        cm = job.getAttributeNode(month).value
                    except AttributeError:
                        cm = ""
                    else:
                        if cm == "1":
                            if months == '':
                                months += '{0}'.format(month)
                            else:
                                months += ', {0}'.format(month)


                # DAYS_AND_OR == OR
                if job_days_and_or == 'OR' and not (job_days in ['-', 'ALL'] or job_weekdays in ['-', 'ALL', '1,2,3,4,5,6,0', '0,1,2,3,4,5,6']):
                    self.__LOG__.warning("Day OR date condition for job (Job:{0}, weekday:{1}, date:{2})".format(job_name, self.__jour__(job_weekdays), job_days))

                    self.warnings.append(
                        {
                            'job': job_name,
                            'reason': "Condition de \"jour OU date\".<br>jour: <tt>{0}</tt> - date : <tt>{1}</tt>".format(self.__jour__(job_weekdays), job_days),
                            'risk': 'La commande sera ex&eacute;cut&eacute; tous les jours correspondants.'
                            }
                        )

                # command line containing libmemsym variable
                if cmdline != '' and len(self.libmemsym_values) > 0:
                    for part in re.split('(\%\%[0-9a-zA-Z_]*)', cmdline):
                        if '%%' in part[:2]:
                            # Store used variables
                            try:
                                self.libmemsym_used[part] = self.libmemsym_used[part] + 1
                            except KeyError:
                                self.libmemsym_used[part] = 1
                                
                            if not part.strip() in self.libmemsym_values and not part.strip() in self.libmemsym_builtin_values:
                                self.__LOG__.error("Command contains reference to unknown libmemsym variable (Job:{0}, variable:{2}, cmd:{1})".format(job_name, cmdline, part.strip()))

                                self.errors.append(
                                    {
                                        'job': job_name,
                                        'reason': "La commande contient une r&eacute;f&eacute;rence &agrave; non d&eacute;finie dans le fichier libmemsym.<br>variable: <tt>{0}</tt><br>cmd: <tt>{1}</tt>".format(part.strip(), cmdline),
                                        'risk': 'La variable sera remplac&eacute;e par une valeur nulle.'
                                        }
                                    )


                # command line containing node name
                if cmdline != '' and len(self.nodes) > 0:
                    for servername in self.nodes:
                        if re.compile(servername).search(cmdline.lower()):
                            self.__LOG__.warning("Command contains hard reference to server name (Job:{0}, servername:{2}, cmd:{1}). Be carefull if you change the environment !!!".format(job_name, cmdline, servername))

                            self.warnings.append(
                                {
                                    'job': job_name,
                                    'reason': "La commande contient le nom d'un serveur.<br>servername:<tt>{1}</tt><br>cmd:<tt>{0}</tt>".format(cmdline, servername),
                                    'risk': 'Si la cha&icirc;ne est d&eacute;ploy&eacute;e sur un autre environnement, il faudra penser &agrave; changer la commande en cons&eacute;quence. <b><span style="color:#A00000;">&Agrave; ajouter imp&eacute;rativement &agrave; la PTIO</span></b>'
                                    }
                                )


                # command line but no nodeid (execution on server)
                if cmdline != '' and (job_nodeid == "-" or job_nodeid == ""):
                    self.__LOG__.warning("Command defined but there is no NODEID. Will be executed on ControlM server itself (Job:{0}, cmd:{1})".format(job_name, cmdline))

                    self.warnings.append(
                        {
                            'job': job_name,
                            'reason': "Commande d&eacute;finie sans NODEID.<br>cmd: <tt>{0}</tt>".format(cmdline),
                            'risk': 'La commande sera ex&eacute;cut&eacute;e sur le serveur Control-M.'
                            }
                        )

                # command line containing & or &&
                if '&&' in cmdline:
                    self.__LOG__.warning("Command containing && (Job:{0}, cmd:{1})".format(job_name, cmdline))
                    self.warnings.append(
                        {
                            'job': job_name,
                            'reason': "La commande contient des &&<br>cmd:<tt>{0}</tt>".format(cmdline),
                            'risk': 'Le code retour de la premi&egrave;re commande ne sera ignor&eacute; par Control-M'
                            }
                        )

                elif '&' in cmdline:
                    # test if it is just stderr redirection
                    if not('2>&1' in cmdline and len(cmdline.split('&'))==2):
                        self.__LOG__.warning("Command containing & (Job:{0}, cmd:{1})".format(job_name, cmdline))
                        self.warnings.append(
                            {
                                'job': job_name,
                                'reason': "La commande contient des &<br>cmd:<tt>{0}</tt>".format(cmdline),
                                'risk': "Le code retour de la commande sera toujours 0 et en cas d'erreur, Control-M ne remontera pas d'erreur."
                                }
                            )
                        

                # MAXWAIT = 99 / for reporting purpose
                if maxwait != '99':
                    self.__LOG__.error("MAXWAIT != 99 (Job:{0}, MAXWAIT:{1})".format(job_name, maxwait))

                    self.errors.append(
                        {
                            'job': job_name,
                            'reason': "MAXWAIT != 99<br>MAXWAIT: <tt>{0}</tt>".format(maxwait),
                            'risk': 'La valeur de MAXWAIT doit &ecirc;tre 99 pour utiliser le reporting <a href="https://si-reportctm.edf.fr">https://si-reportctm.edf.fr</a>.'
                            }
                        )



                # Conformity of group name
                if group[0] not in ['N', 'W', 'F', 'X'] or len(group) > 5:
                    self.__LOG__.warning("GROUP NAME not compliant (Job:{0}, Group:{1})".format(job_name, group))

                    self.warnings.append(
                        {
                            'job': job_name,
                            'reason': "Nom de groupe non conforme<br>GROUP: <tt>{0}</tt>".format(group),
                            'risk': '<b>Non respect de la norme</b><br>Le nom du groupe doit &ecirc;tre compos&eacute; de 5 caract&egrave;res se d&eacute;composant comme suit :<ul><li>1 caract&egrave;re repr&eacute;sentatif d\'un traitement parmi N, W, F, X</li><ul><li>N : Traitement normal</li><li>W : Traitement de secours</li><li>F : Traitement fant&ocirc;me</li><li>X : Traitement d\'exploitation</li></ul><li>4 caract&egrave;res alphanum&eacute;riques correspondant &agrave; la fonction du groupe</li></ul>'
                            }
                        )



                # Conformity of job name
                if job_name[0:5] != group or len(job_name) < 8 or len(job_name) > 8 and (job_name[8] != '-' or len(job_name) > 32):
                    self.__LOG__.warning("JOB NAME not compliant (Job:{0})".format(job_name))

                    self.warnings.append(
                        {
                            'job': job_name,
                            'reason': "Nom de job non conforme<br>JOB: <tt>{0}</tt><br>GROUP: <tt>{1}</tt>".format(job_name, group),
                            'risk': '<b>Non respect de la norme</b><br>Le nom du job doit &ecirc;tre compos&eacute; de la mani&agrave;re suivante :<ul><li>5 caract&egrave;res correspondant au nom du groupe associ&eacute;</li><li>3 caract&egrave;res libres correspondant &agrave;  la fonction du groupe</li><li>24 caract&egrave;res maxi optionnels d&eacute;butant par le caract&egrave;re - (tiret)</li></ul>'
                            }
                        )


                # Job Type
                if task_type == 'Job':
                    self.__LOG__.warning("Tasktype == Job (Job:{0})".format(job_name))

                    self.warnings.append(
                        {
                            'job': job_name,
                            'reason': "Type de t&acirc;che Job et non Command<br>JOB: <tt>{0}</tt><br>GROUP: <tt>{1}</tt>".format(job_name, group),
                            'risk': 'Non ma&icirc;tris&eacute; : l\'appel de la commande n\'est pas le m&ecirc;me'
                            }
                        )


                jobdesc = {
                    'name': job_name,
                    'cmdline': cmdline,
                    'inline_script': inline_script,
                    'use_inline_script': use_inline_script,
                    'owner': owner,
                    'desc': desc,
                    'group': group,
                    'days': job_days,
                    'months': months,
                    'weekdays': job_weekdays,
                    'days_and_or': job_days_and_or,
                    'input': inc,
                    'output': outc,
                    'oncondition': onc,
                    'nodeid': job_nodeid,
                    'tasktype': task_type,
                    'confirm': confirm,
                    'cyclic': cyclic,
                    'appl_form': appl_form,
                    'appl_type': appl_type
                    }

                self.__LOG__.debug("Job (Job:{0})".format(pprint.pformat(jobdesc)))

                if cyclic == "1":
                    jobdesc['interval'] = '{0} ({1})'.format(interval, cyclic_type)
                else:
                    jobdesc['interval'] = '-'
                    
                if job_time == '-':
                    jobdesc['time'] = '-'
                else:
                    jobdesc['time'] = '{0}:{1}'.format(str(job_time)[0:2], str(job_time)[2:])


                if not jobdesc['group'] in jobs:
                    jobs[jobdesc['group']] = []
                jobs[jobdesc['group']].append(jobdesc)

            chain.append([(chain_name, date_last_upload), jobs])

            # Conformity of NodeID name
            #  Le nom du Node ID doit être composé de 10 caractères se décomposant comme suit :
            # - 1 caractère représentant la direction [D]
            # - 3 caractères alphanumériques désignant le trigramme de l'application 
            # - 3 caractères alphanumériques d'environnement 
            # - 3 caractères alphanumériques d'incrément ou d'OS
            for nodeid in nodeid_list:
                if len(nodeid) != 10 or nodeid[0:5] != chain_name[0:5]:
                    self.__LOG__.warning("NODE_ID NAME not compliant (NODE_ID:{0})".format(nodeid))

                    self.warnings.append(
                        {
                            'job': '-',
                            'reason': "Nom de NODE_ID non conforme<br>NODE_ID: <tt>{0}</tt>".format(nodeid),
                            'risk': '<b>Non respect de la norme</b><br>Le nom du Node ID doit &ecirc;tre compos&eacute; de 10 caract&egrave;res se d&eacute;composant comme suit :<ul><li>1 caract&egrave;re repr&eacute;sentant la direction</li><li>3 caract&egrave;res alphanum&eacute;riques d&eacute;signant le trigramme de l\'application</li><li>3 caract&egrave;res alphanum&eacute;riques d\'environnement(ex: 21P) </li><li>3 caract&egrave;res alphanum&eacute;riques d\'incr&eacute;ment ou d\'OS.</li></ul>'
                            }
                        )
                
            
            # log nodeID list
            self.__LOG__.info("NODEID list : {0}".format(', '.join(nodeid_list)))

        # log debug whole chain
        self.__LOG__.debug("chain: {0}".format(pprint.pformat(chain)))
                            
        return chain




    def convert_chain_to_dot(self, chains, group_to_convert=None, descriptions=False):
        """
        Converts the chain description list from read_xml as a Graphviz dot file

        Returns Graphviz dot file as a string
        """
        self.__LOG__.debug("** ControlM::convert_chain_to_dot")


        # Get all input and output conditions
        ic = []   # input conditions
        oc = []   # output conditions
        onc = []  # on condition
        icd = {}  # input conditions dictionary
        ocd = {}  # output conditions dictionary
        #oncd = {} # on condition dictionary
        jocs = {} # job output conditions sign
        all_jobs = {}  # by jobs
        seen_jobs = []
        for chain in chains:
            for groups in chain[1:]:
                for group in groups.keys():
                    self.__LOG__.debug("Group : {0}".format(group))
                    for job in groups[group]:
                        job_name = job['name']
                        all_jobs[job_name] = {'ic':[], 'oc':[], 'iccond':{}, 'onc':[]}
                        for nb in range(len(job['input'])):
                            ic.append(job['input'][nb]['name'])
                            #icd[job['input'][nb]['name']] = job_name
                            # many icd by job
                            try:
                                icd[job['input'][nb]['name']].append(job_name)
                            except KeyError:
                                icd[job['input'][nb]['name']] = [job_name]

                            all_jobs[job_name]['ic'].append(job['input'][nb]['name'])
                            # get input condition
                            all_jobs[job_name]['iccond'][job['input'][nb]['name']] = job['input'][nb]['cond']
                        for nb in range(len(job['output'])):
                            oc.append(job['output'][nb]['name'])
                            if job['output'][nb]['sign'] in ['ADD', '+']:
                                #ocd[job['output'][nb]['name']] = job_name
                                # many ocd by job
                                try:
                                    ocd[job['output'][nb]['name']].append(job_name)
                                except KeyError:
                                    ocd[job['output'][nb]['name']] = [job_name]

                            all_jobs[job_name]['oc'].append(job['output'][nb]['name'])
                            jocs['{0}/{1}'.format(job_name, job['output'][nb]['name'])] = job['output'][nb]['sign']

                        for nb in range(len(job['oncondition'])):
                            for nbdocond in range(len(job['oncondition'][nb]['docond'])):
                                onc.append(job['oncondition'][nb]['docond'][nbdocond]['docode'])
                                all_jobs[job_name]['onc'].append(job['oncondition'][nb]['docond'][nbdocond]['docode'])

        self.__LOG__.debug("icd:{0}".format(pprint.pformat(icd)))
        self.__LOG__.debug("ocd:{0}".format(pprint.pformat(ocd)))
        self.__LOG__.debug("onc:{0}".format(pprint.pformat(onc)))
        self.__LOG__.debug("all_jobs:{0}".format(pprint.pformat(all_jobs)))
                            


        dot = '''/*------------------------------------------------------*/
/* Control-M chain                                      */
/*------------------------------------------------------*/
/* Compile with Graphviz: see http://www.graphviz.org/  */
/*------------------------------------------------------*/
/* This chain was created with ControlM.py              */
/* by Alexandre Norman (norman@xael.org)                */
/* See: http://hg.xael.org/control-m/wiki/Home          */
/*------------------------------------------------------*/
// Version: {0}\n\n\n'''.format(__version__)
        for chain in chains:
            chain_name = chain[0]
            dot += 'digraph G {\n'
            dot += 'charset=utf8\n'
            dot += '#label="Done with http://hg.xael.org/control-m/";\n'
            dot += 'node [shape=record];\n'
            dot += 'graph [\n'
            dot += 'labeljust=r,\n'
            dot += 'fontcolor=gray,\n'
            #dot += 'label="{0}\\nDone with http://hg.xael.org/control-m/",\n'.format(chain_name)
            dot += 'label="{0}",\n'.format(chain_name)
            dot += ']\n'

            ## # Legende
            ## dot += '''
            ## subgraph cluster_legend {
            ## labeljust=l;
            ## fontcolor=black;
            ## node [style=filled,shape=record];
            ## style=filled;
            ## color=lightgrey;
            ## "dummy" [shape=record,fillcolor=yellow,label="{Dummy job}"]
            ## "confirm " [shape=record,fillcolor=pink,label="{Job to confirm}"]
            ## "normal" [shape=record,fillcolor=white,label="{Normal job}"]
            ## label="Legend";
            ## }
            ## '''

            for groups in chain[1:]:
                if group_to_convert is None:
                    mygroups = groups.keys()
                else:
                    mygroups = [group_to_convert]
                self.__LOG__.debug("Groups to convert : {0}".format(mygroups))
                for group in mygroups:
                    self.__LOG__.debug("Group : {0}".format(group))
                    dot += 'subgraph cluster_{0} {{\n'.format(group)
                    dot += 'labeljust=l;\n'
                    dot += 'fontcolor=black;\n'
                    dot += 'node [style=filled,shape=record];\n'
                    dot += 'style=filled;\n'
                    dot += 'color=lightgrey;\n'        


                    for job in groups[group]:
                        job_name = job['name']
                        seen_jobs.append(job_name)
                        self.__LOG__.debug("Job : {0}".format(job_name))
                        
                        # Valid Tasktype :
                        # Job
                        # Detached
                        # Command
                        # Dummy
                        # External
                        
                        # jobs &agrave; confirmation manuelle en rose
                        if job['confirm'] == '1':
                            dot += '"{0}" [shape=record,fillcolor=pink,label="{1}"]\n'.format(job_name, self.__job_desc__(job, descriptions=descriptions))
                        # Dummy en jaune
                        elif job['tasktype'] == "Dummy":
                            dot += '"{0}" [shape=record,fillcolor=yellow,label="{1}"]\n'.format(job_name, self.__job_desc__(job, descriptions=descriptions))
                        # jobs cycliques en vert
                        elif job['cyclic'] == '1':
                            dot += '"{0}" [shape=record,fillcolor=green,label="{1}"]\n'.format(job_name, self.__job_desc__(job, descriptions=descriptions))
                        elif job['appl_form'] != '' or (job['appl_type'] != '' and job['appl_type'] != 'OS'):
                            dot += '"{0}" [shape=record,fillcolor=purple,label="{1}"]\n'.format(job_name, self.__job_desc__(job, descriptions=descriptions))
                        else:
                            dot += '"{0}" [shape=record,fillcolor=white,label="{1}"]\n'.format(job_name, self.__job_desc__(job, descriptions=descriptions))


                    dot += 'label="{0}";\n'.format(group)
                    dot += '}\n'

                # links beetwens jobs
                cic = []    # control input conditions
                coc = []    # control output conditions
                links = []
                links_delete = []
                for job_name in seen_jobs:
                    for input_cond in all_jobs[job_name]['ic']:
                        for k in ocd:
                            if k[:-2] == input_cond[:-2]:
                                for j in ocd[k]:
                                    if (j, job_name, input_cond) not in links:
                                        links.append((j, job_name, input_cond))

                                cic.append(input_cond)

                    for output_cond in all_jobs[job_name]['oc']:
                        for k in icd:
                            if k[:-2] == output_cond[:-2]:
                                coc.append(output_cond)
                                if jocs['{0}/{1}'.format(job_name, output_cond)] == 'DEL':
                                    for j in icd[k]:
                                        if (job_name, j, output_cond) not in links_delete:
                                            links_delete.append((job_name, j, output_cond))

                    for output_cond in all_jobs[job_name]['onc']:
                        for k in icd:
                            if k == output_cond:
                                coc.append(output_cond)
                                for j in icd[k]:
                                    if (job_name, j, output_cond) not in links:
                                        links.append((job_name, j, output_cond))


                self.__LOG__.debug("cic:{0}".format(pprint.pformat(cic)))
                self.__LOG__.debug("coc:{0}".format(pprint.pformat(coc)))
                self.__LOG__.debug("links:{0}".format(pprint.pformat(links)))
                self.__LOG__.debug("links_delete:{0}".format(pprint.pformat(links_delete)))


                for couple in links:
                    job_name = couple[0]
                    next_job_name = couple[1]
                    condition = couple[2]

                    andoror = all_jobs[next_job_name]['iccond'][condition]
                    if andoror != 'AND':
                        okko = condition.lower()[-2:]
                        if okko == "ok":
                            dot += "edge [color=blue];\n"
                            dot += '"{0}" -> "{1}" [label="{2}", fontcolor=blue];\n'.format(job_name, next_job_name, andoror)
                            dot += "edge [color=black];\n"
                        else:
                            dot += "edge [color=purple];\n"
                            dot += '"{0}" -> "{1}" [label="{2}", fontcolor=purple];\n'.format(job_name, next_job_name, '{0} ({1})'.format(okko, andoror))
                            dot += "edge [color=black];\n"
                    else:
                        okko = condition.lower()[-2:]
                        if okko == "ok":
                            dot += '"{0}" -> "{1}" [fontcolor=black];\n'.format(job_name, next_job_name)
                        else:
                            dot += '"{0}" -> "{1}" [label="{2}", fontcolor=red];\n'.format(job_name, next_job_name, okko)
                            dot += "edge [color=black];\n"


                for couple in links_delete:
                    job_name = couple[0]
                    next_job_name = couple[1]
                    condition = couple[2]

                    okko = condition.lower()[-2:]
                    if okko == "ok":
                        dot += "edge [color=red];\n"
                        dot += '"{0}" -> "{1}" [label="DEL", fontcolor=red];\n'.format(job_name, next_job_name, condition)
                        dot += "edge [color=black];\n"
                    else:
                        dot += "edge [color=red];\n"
                        dot += '"{0}" -> "{1}" [label="DEL ({2})", fontcolor=red];\n'.format(job_name, next_job_name, okko)
                        dot += "edge [color=black];\n"

            dot += '}\n\n'


                            
            # check that all inputs are linked to outputs
            for inp in ic:
                if inp not in coc:
                    inp_job_name = ''
                    for job_name in all_jobs:
                        if inp in all_jobs[job_name]['onc']:
                            inp_job_name = job_name
                            break

                        if inp in all_jobs[job_name]['oc']:
                            inp_job_name = job_name
                            break


                    # find job name input
                    jn = ''
                    for job_name in all_jobs:
                        if inp in all_jobs[job_name]['ic']:
                            jn = job_name
                            break


                    if group_to_convert is None:
                        self.__LOG__.error("Input condition {0} (job {1}) not linked to any output condition".format(inp, jn))

                        self.errors.append(
                            {
                                'job': inp_job_name,
                                'reason': "La condition d'entr&eacute;e <tt>{0} (job {1})</tt> n'est reli&eacute;e &agrave; aucune condition de sortie.".format(inp, jn),
                                'risk': 'Au chargement de la cha&icirc;ne, le job sera ex&eacute;cut&eacute; directement.'
                                }
                            )

            # check that all outputs are linked to inputs
            for out in oc:
                if out not in cic:
                    out_job_name = ''
                    for job_name in all_jobs:
                        if out in all_jobs[job_name]['ic']:
                            out_job_name = job_name
                            break

                    # find job name from output
                    jn = ''
                    for job_name in all_jobs:
                        if out in all_jobs[job_name]['oc']:
                            jn = job_name
                            break



                    if group_to_convert is None:
                        self.__LOG__.warning("Output condition {0} (job {1}) not linked to any input condition".format(out, jn))

                        self.warnings.append(
                            {
                                'job': job_name,
                                'reason': "La condition de sortie <tt>{0}  (job {1})</tt> n'est pas reli&eacute;e &agrave; une condition d'entr&eacute;e.".format(out, jn),
                                'risk': ''
                                }
                            )

        return dot




    def convert_chain_to_csv(self, chains):
        """
        Converts the chain description list from read_xml as a CSV file

        Returns CSV file as a string
        """
        self.__LOG__.debug("** ControlM::convert_chain_to_cvs")

        # Get all input and output conditions
        ic = []   # input conditions
        oc = []   # output conditions
        onc = []  # on conditions
        jocs = {} # job output conditions sign
        all_jobs = {}  # by jobs
        for chain in chains:
            for groups in chain[1:]:
                for group in groups.keys():
                    self.__LOG__.debug("Group : {0}".format(group))
                    for job in groups[group]:
                        job_name = job['name']
                        all_jobs[job_name] = {'ic':[], 'oc':[], 'iccond':{}, 'icop':{}, 'onc':[]}
                        for nb in range(len(job['input'])):
                            ic.append(job['input'][nb]['name'])
                            all_jobs[job_name]['ic'].append(job['input'][nb]['name'])
                            # get input condition
                            all_jobs[job_name]['iccond'][job['input'][nb]['name']] = job['input'][nb]['cond']
                            all_jobs[job_name]['icop'][job['input'][nb]['name']] = job['input'][nb]['op']
                        for nb in range(len(job['output'])):
                            oc.append(job['output'][nb]['name'])
                            all_jobs[job_name]['oc'].append(job['output'][nb]['name'])
                            jocs['{0}/{1}'.format(job_name, job['output'][nb]['name'])] = job['output'][nb]['sign']
                        for nb in range(len(job['oncondition'])):
                            onc_name = job['oncondition'][nb]['name']
                            onc_stmt = job['oncondition'][nb]['stmt']
                            onc_do = job['oncondition'][nb]['do']
                            for nbdocond in range(len(job['oncondition'][nb]['docond'])):
                                lonc = job['oncondition'][nb]['docond'][nbdocond]
                                self.__LOG__.debug('{0}'.format(pprint.pformat(lonc)))
                                all_jobs[job_name]['onc'].append('name:{0} / stmt:{1} / docond:{2} / do:{3} '.format(
                                        onc_name,
                                        onc_stmt,
                                        lonc,
                                        onc_do,
                                        ))

        csv = ''
        for chain in chains:
            csv += '{1}"{0}";"Date upload : {2}";\n'.format(chain[0][0], codecs.BOM_UTF8.decode('utf-8'), chain[0][1])
            csv += '"Groupe";"Tasktype";"Job";"heure";"jour";"mois";"Description";"User";"NodeID";"Cmdline";"Use inline script";"inline script";"Job cyclique";"Intervalle";"Confirmation manuelle";"Condition entree";"Condition sortie";"Conditions forcées";\n'
            for groups in chain[1:]:
                for group in groups.keys():
                    self.__LOG__.debug("Group : {0}".format(group))

                    for job in groups[group]:
                        job_name = job['name']
                        self.__LOG__.debug("Job : {0}".format(job_name))

                        input_conds_l = []
                        for ipc in all_jobs[job_name]['ic']:
                            op = all_jobs[job_name]['icop'][ipc]
                            if op == '(' or op == 'A(' or op == 'O(':
                                input_conds_l.append('( {0} [{1}]'.format(ipc, all_jobs[job_name]['iccond'][ipc]))
                            elif op == ')':
                                input_conds_l.append('{0} [{1}] )'.format(ipc, all_jobs[job_name]['iccond'][ipc]))
                            else:
                                input_conds_l.append('{0} [{1}]'.format(ipc, all_jobs[job_name]['iccond'][ipc]))

                        output_conds_l = []
                        for opc in all_jobs[job_name]['oc']:
                            output_conds_l.append('{0} ({1})'.format(opc, jocs['{0}/{1}'.format(job_name, opc)]))

                        forced_conds_l = all_jobs[job_name]['onc']
                        
                        input_conds = ' / '.join(input_conds_l)
                        output_conds = ' / '.join(output_conds_l)
                        forced_conds = ' / '.join(forced_conds_l)
                        
                        csv += self.__job_desc_csv__(job) + '"{0}";"{1}";"{2}";'.format(input_conds, output_conds, forced_conds) + '\n'

        return csv




    def convert_dot_to_png(self, dot_chain):
        """
        Calls Graphviz (dot) to create png file

        Returns png bytes
        """
        self.__LOG__.debug("** ControlM::convert_dot_to_png")

        cmd = "dot -Tpng"
        p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if sys.version_info[0] > 2:
            bdot_chain = bytes(dot_chain, 'utf-8')
        else:
            bdot_chain = dot_chain

        (png, p_err) = p.communicate(bdot_chain)

        if len(p_err)>0:
            raise ValueError('Problem with convert_dot_to_png :\n{0}'.format(p_err))

        return png



    def __jour__(self, day):
        """
        Converts day numbers to french abreviations
        """
        log = logging.getLogger("ControlM")

        jour = {
            '0': 'dim',
            '1': 'lun',
            '2': 'mar',
            '3': 'mer',
            '4': 'jeu',
            '5': 'ven',
            '6': 'sam',
            'ALL': 'tous',
            '1,2,3,4,5,6,0': 'tous',
            '0,1,2,3,4,5,6': 'tous',
            '-': 'sans'
            }

        jours = ''
        if day not in jour:
            ljours = []
            for j in day.split(','):
                try:
                    ljours.append(jour[j])
                except KeyError:
                    log.error("Formatting day : {0}".format(day))
            jours = ','.join(ljours)
        else:
            jours = jour[day]
        
        return jours



    def __job_desc__(self, job, descriptions=False):
        """
        Returns job description for Graphviz use
        """

        val = [('', job['name']), ('Jour', '{0} {2} ({1})'.format(self.__jour__(job['weekdays']), job['days'], job['days_and_or'])), ('Horaire', job['time']), ('Cyclique', job['interval']), ('NodeID', job['nodeid'])]

        desc = ''

        # SAP stuff and others
        if job['appl_form'] != '' and job['appl_type'] != '':
            val.append(('appl', '{0}::{1}'.format(job['appl_type'], job['appl_form'])))
        elif job['appl_form'] != '':
            val.append(('appl_form', job['appl_form']))
        elif job['appl_type'] != '':
            val.append(('appl_type', job['appl_type']))

        if job['use_inline_script'] == 'Y':
            val.append(('inline script', 'Y'))

        # confirmation manuelle
        if job['confirm'] == '1':
            val.append(('Confirmation manuelle', 'oui'))


        if descriptions == True:
            try:
                #val.append(('', job['desc'].decode('utf-8', errors='replace').encode('ascii', errors='replace').replace('|', '&#124;').replace('"', '&#34;"').replace('>', '&#62;').replace('<', '&#60;')))
                val.append(('', job['desc'].replace('|', '&#124;').replace('"', '&#34;').replace('>', '&#62;').replace('<', '&#60;')))
            except UnicodeDecodeError:
                val.append(('', 'desc unavailable...'))
                __LOG__.debug("Description undecodable [JOB : {0} / {1}]".format(job['name'], job['desc']))
            
        for v in val:
            if v[1] != '-':
                if desc != '':
                    desc += '| '
                else:
                    desc += '{ '
                if v[0] != '':
                    desc += '{0} : {1} '.format(v[0], v[1])
                else:
                    desc += '{0} '.format(v[1])

        desc += ' }'
        return desc



    def __job_desc_csv__(self, job):
        """
        Returns job description for CSV use
        """
        desc = '"{0}";"{1}";"{2}";"{3}";"{4}";"{5}";"{6}";"{7}";"{8}";"{9}";"{10}";"{11}";"{12}";"{13}";"{14}";'.format(
            job['group'],
            job['tasktype'],
            job['name'],
            job['time'],
            '{0} {2} ({1})'.format(self.__jour__(job['weekdays']), job['days'], job['days_and_or']),
            job['months'],
            job['desc'],
            job['owner'],
            job['nodeid'], 
            job['cmdline'].replace('"', '""'),
            job['use_inline_script'],
            job['inline_script'].replace('"', '""'),
            job['cyclic'],
            job['interval'],
            job['confirm'] == '1',
            )

        return desc

############################################################################

__LOG__ = None

############################################################################

def __init_log_to_sysout__(level=logging.INFO):
    """
    """
    global __LOG__
    logger = logging.getLogger("ControlM")
    logger.setLevel(level)
    fh = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    __LOG__ = logging.getLogger("ControlM")
    return

############################################################################

def __show_version__(name, **kwargs):
    """
    Show version
    """
    import os
    print("{0} version {1}".format(os.path.basename(name), __version__))
    return True


############################################################################


@clize.clize(
    alias = {
        'csv': ('c',),
        'dot': ('d',),
        'descriptions': ('D',),
        'png': ('p',),
        'groups': ('g',),
        },
    extra = (
        clize.make_flag(
            source=__show_version__,
            names=('version', 'v'),
            help="Show the version",
            ),
        )
    )
def __main__(controlm_file, csv='', dot='', png='', groups=False, descriptions=False, debug=False):
    """
    ControlM.py
    
    controlm: name of control-M schedtable.xml or control-M tar archive

    cvs: output CSV filename

    dot: output graphviz dot filename
    
    png: output PNG filename

    groups: dot files or png files by group

    descriptions: jobs description in graphviz

    debug: debug

    Extract control-M schedtable.xml from tar file if [controlm-file] is a tar file or
    directly parse [controlm-file] to create output suitable for graphviz (with dot).

    If [dot] is given, send graphiz to given file, otherwise send to stdout.

    If [png] is given, dot is called to produce png output to given file.

    Example :
    python ControlM.py DATA00I13_DEVBEX01.tar --png DATA00I13.png

    Written by : Alexandre Norman <norman at xael.org>
    """
    global __LOG__
    if debug:
        __init_log_to_sysout__(logging.DEBUG)
    else:
        __init_log_to_sysout__()

    cm = ControlM()

    chain = cm.read_xml(controlm_file)
    dotout = cm.convert_chain_to_dot(chain, descriptions=descriptions)

    if dot != '':
        __LOG__.debug("Write result of convert_chain_to_dot to [{0}].".format(dot))
        with open(dot, 'w') as f:
            f.write(dotout)

    if png != '':
        __LOG__.debug("Write result of convert_dot_to_png to [{0}].".format(png))
        with open(png, 'wb') as f:
            png_data = cm.convert_dot_to_png(dotout)
            f.write(png_data)

    if groups:
        for cgroup in chain[0][1:]:
            for group in cgroup.keys():
                dotout = cm.convert_chain_to_dot(chain, group, descriptions=descriptions)
                if dot != '':
                    if dot[-4:] == ".dot":
                        gdot = "{0}_{1}.dot".format(dot[:-4], group)
                    else:
                        gdot = "{0}_{1}.dot".format(dot, group)

                    __LOG__.debug("Write result of convert_chain_to_dot to [{0}] (group {1}).".format(gdot, group))
                    with open(gdot, 'w') as f:
                        f.write(dotout)

                if png != '':
                    if png[-4:] == ".png":
                        gpng = "{0}_{1}.png".format(png[:-4], group)
                    else:
                        gpng = "{0}_{1}.png".format(png, group)
                        
                    __LOG__.debug("Write result of convert_dot_to_png to [{0}] (group {1}).".format(gpng, group))
                    with open(gpng, 'wb') as f:
                        png_data = cm.convert_dot_to_png(dotout)
                        f.write(png_data)

    if csv != '':
        __LOG__.debug("Write result of convert_dot_to_csv to [{0}].".format(csv))
        with open(csv, 'w', encoding='utf-8') as f:
            f.write(cm.convert_chain_to_csv(chain))



    __LOG__.debug("All done. Exiting.")
    
    return


############################################################################

__all__ = ['ControlM']


# MAIN -------------------
if __name__ == '__main__':

    clize.run(__main__)
    sys.exit(0)

    
#<EOF>######################################################################

