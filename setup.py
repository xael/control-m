# setup.py
from distutils.core import setup

import sys

if sys.platform == 'win32':
    import py2exe

import ControlM


setup(
    name = 'Analyse_Control-M',
    version = ControlM.__version__,
    author = 'Alexandre Norman',
    author_email = 'norman@xael.org',
    license ='gpl-3.0.txt',
    keywords='Control-M, job scheduler, analysis tool',
    # Get more strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    platforms=[
        'Operating System :: OS Independent',
        ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Programming Language :: Python',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Quality Assurance',
        'Topic :: Software Development :: Documentation',
        ],
    py_modules=['web_service', 'ControlM'],
    url = 'http://hg.xael.org/control-m/wiki/Home',
    description = 'This is a python script which allows to extract informations from scheduling tables created with BMC Control-M.',
    long_description=open('README.rst').read() + '\n' + open('CHANGELOG').read(),
    )

