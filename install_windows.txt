Installation
============

Fichiers nécessaires
--------------------
 - Installeur Python (msi téléchargeable sur https://www.python.org/download/)
 - Installeur Graphviz (msi téléchargeable sur http://www.graphviz.org/Download_windows.php)
 - Zip Clize (Zip téléchargeable sur https://github.com/epsy/clize)
 - Dépendances Six pour Clize (Fichier whl téléchargeable sur https://pypi.python.org/pypi/six)
 - Dépendances Sigtools pour Clize (Fichier whl téléchargeable sur https://pypi.python.org/pypi/sigtools)
 - Zip xael-ControlM (Zip téléchargeable sur http://hg.xael.org/control-m/downloads)

Installation
------------
 - Installer l'interpréteur Python
 - Installer Graphviz
 - Installer les dépendences pour Clize, en ligne de commande se placer dans le répertoire contenant les fichiers whl puis lancer : 

    pip install six-1.7.2-py2.py3-none-any.whl
    pip install sigtools-0.1a3-py33-none-any.whl

Extraire l'archive clize-master.zip puis en ligne de commande se placer dans le répertoire clize-master puis lancer : 
 python setup.py install

Extraire l'archive xael-control-m-XXXXXXXXX.zip

Lancement
---------
En ligne de commande se placer dans le répertoire xael-control-m-XXXXXXXXX.

** Syntaxe : **

Extraction du png avec description des jobs : 

  python ControlM.py <path_to_tar_CTM> -p <path_to_png_dest> -D

Extraction du png, du csv et du fichier de warning et d'erreurs : 

  python ControlM.py <ctm_tar_in> -p <png_out> -D -c <csv_out> 2> <errors_and_warnings_out>

** Exemples : **

  python ..\xael-control-m-133198d39961\ControlM.py DSFENAT01_DEVBEX01_BBR3ZYfd07EQpJH7.tar -p DSFENAT01.png -D -c DSFENAT01.csv 2> DSFENAT01_err_and_warn.txt


