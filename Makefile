VERSION=$(shell python setup.py --version)
ARCHIVE=$(shell python setup.py --fullname)
SOURCES:=$(shell grep '.py$$' MANIFEST|egrep -v 'bottle.py|setup.py')

manifest:
	@python setup.py sdist --manifest-only

install:
	@python setup.py install

correctness:
	echo $(SOURCE)
	pylint --rcfile=.pylintrc $(SOURCES)
	pep8 --ignore=E501 $(SOURCES)

archive:
	@python setup.py sdist
	@echo Archive is create and named dist/$(ARCHIVE).tar.gz
	@echo -n md5sum is :
	@md5sum dist/$(ARCHIVE).tar.gz

license:
	@python setup.py --license

register:
	@python setup.py register

