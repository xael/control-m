#!/usr/bin/env python
# -*- coding: utf-8-unix -*-

"""
web_engine.py
http://bottlepy.org/docs/stable/

Author : Alexandre Norman - norman@xael.org
"""

import os
import sys
import platform
import threading
import shutil

import bottle

import uuid

import locale
import zipfile

import logging
import ControlM

import datetime
import traceback


if sys.platform != 'win32':
    import pwd
    import grp


try:
    import clize
except ImportError:
    print("This program uses clize. See : https://github.com/epsy/clize")
    sys.exit(1)


WENG = bottle.Bottle()
WENG.avwa = bottle
WENG.users_sessions = {}



def log(func):
    """
    Debug decorator
    """
    def wrapper(*args, **kwargs):
        """
        Wrapper for log string
        """
        logger = logging.getLogger("WS_CA")
        logger.debug("{3}::{0} / {1} / {2}".format(func.__name__, args, kwargs, func.__module__))
        res = func(*args, **kwargs)
        return res
    return wrapper    


@log
def WebEngine(port=8080, host='127.0.0.1', debug=False):
    """
    Starting webserver and wait for connexions

    Arguments:
    - `port` : port to listen
    - `host` : address to listen
    """
    WENG.port = port
    WENG.host = host

    logger = logging.getLogger("WS_CA")

    if sys.platform != "win32" and (os.getuid() == 0 or os.getgid() == 0):
        logger.error('Running as root ! Aborting !')
        return

    
    print("connect to http://{0}:{1}".format(WENG.host, WENG.port))
    try:
        import cherrypy
        WENG.run(host=WENG.host, port=WENG.port, server='cherrypy', debug=debug, reloader=False)
    except ImportError:
        logger.error('Could not find CherryPy web framework (http://www.cherrypy.org/). Going to internal bottelpy web server.')
        WENG.run(host=WENG.host, port=WENG.port, debug=debug, reloader=False)
    return



@WENG.route('/static/<filename>')
def server_static(filename):
    """
    Routing static files
    """
    return bottle.static_file(filename, root='./static/')



@WENG.route('/')
@log
def index():
    """
    index, serve form page
    """
    return WENG.avwa.template('formulaire',
                              error=None,
                              footer='<span class="hidden">{0}</span> version : {1} - date : {2}'.format(
                                  ControlM.__author__,
                                  ControlM.__version__,
                                  ControlM.__last_modification__
                                  )
                              )


@WENG.route('/', method='POST')
@log
def analyse_file():
    """
    analyse_file :
    - uploads file to /uploads/UUID/
    - extract schedtable.xml
    - create CSV file and png files for global chain and jobs groups
    - store everything in a tar file
    - serve it back
    """

    data = WENG.avwa.request.files.get('datafile')
    if data is not None:
        filename = data.filename

        gfx_global = True
        gfx_detaille = True
        graphviz = False
        descriptions = False

        gfx_global = WENG.avwa.request.forms.get('gfx_global')
        gfx_detaille = WENG.avwa.request.forms.get('gfx_detaille')
        graphviz = WENG.avwa.request.forms.get('graphviz')
        descriptions = (WENG.avwa.request.forms.get('descriptions') == 'on')

        upload = WENG.avwa.request.files.get('datafile')
        name, ext = os.path.splitext(upload.filename)
        if ext not in ('.tar','.xml'):
            return WENG.avwa.template('formulaire',
                                      filename = filename,
                                      error = 'Extension de fichier non autorisée. Seuls les fichiers en .tar ou .xml sont admis.',
                                      footer = '<span class="hidden">{0}</span> version : {1} - date : {2}'.format(
                                          ControlM.__author__,
                                          ControlM.__version__,
                                          ControlM.__last_modification__
                                          )
                                      )


        cp = "{0}".format(uuid.uuid4())
        path = os.path.join("uploads", cp)
        myfile =  os.path.join(path, upload.filename)
        os.mkdir(path)

        upload.save(myfile)

        files_list = []
            


        logger = logging.getLogger("ControlM")
        logger.setLevel(logging.INFO)
        fh = logging.FileHandler(os.path.join(path, 'errors_and_warnings.txt'), mode='a', encoding='utf-8', delay=False)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s\r\n')
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        files_list.append(os.path.join(path, 'errors_and_warnings.txt'))

        cm = ControlM.ControlM()

        try:
            chain = cm.read_xml(myfile)
        except:
            # Delete files later
            mytimer = threading.Timer(5.0, delete_later, [path], {} )
            mytimer.start()

            error = sys.exc_info()[0]
            import cgi
            error = cgi.escape(str(sys.exc_info()[0])).encode('ascii', 'xmlcharrefreplace')
            return WENG.avwa.template('formulaire',
                                      filename = filename,
                                      error='L\'erreur suivante est apparue lors du traitement : <div class="error">{0}<br>{1}</div>'.format(error, traceback.format_exc()),
                                      footer='<span class="hidden">{0}</span> version : {1} - date : {2}'.format(
                                          ControlM.__author__,
                                          ControlM.__version__,
                                          ControlM.__last_modification__
                                          )
                                      )


        ulogger = logging.getLogger("CM_Usage")
        ulogger.info("traitement : {0}".format(name))

        dot = os.path.join(path, '{0}.dot'.format(name))
        csv = os.path.join(path, '{0}.csv'.format(name))
        png = os.path.join(path, '{0}.png'.format(name))
        groups = True


        dotout = cm.convert_chain_to_dot(chains=chain, group_to_convert=None, descriptions=descriptions)

        if dot != '' and graphviz:
            with open(dot, 'w') as f:
                f.write(dotout)
                files_list.append(dot)

        schema = ''
        if png != '' and gfx_global:
            with open(png, 'wb') as f:
                png_data = cm.convert_dot_to_png(dotout)
                f.write(png_data)
                files_list.append(png)
                schema = '{0}.png'.format(name)

        schema_groupes = []
        if groups and gfx_detaille:
            for cgroup in chain[0][1:]:
                for group in cgroup.keys():
                    dotout = cm.convert_chain_to_dot(chains=chain, group_to_convert=group, descriptions=descriptions)
                    if dot != '' and graphviz:
                        if dot[-4:] == ".dot":
                            gdot = "{0}_{1}.dot".format(dot[:-4], group)
                        else:
                            gdot = "{0}_{1}.dot".format(dot, group)

                        with open(gdot, 'w') as f:
                            f.write(dotout)
                            files_list.append(gdot)

                    if png != '':
                        if png[-4:] == ".png":
                            gpng = "{0}_{1}.png".format(png[:-4], group)
                        else:
                            gpng = "{0}_{1}.png".format(png, group)

                        with open(gpng, 'wb') as f:
                            schema_groupes.append('{0}_{1}.png'.format(name, group))
                            png_data = cm.convert_dot_to_png(dotout)
                            f.write(png_data)
                            files_list.append(gpng)

        if csv != '':
            with open(csv, 'w') as f:
                f.write(cm.convert_chain_to_csv(chain))
                files_list.append(csv)


        reporting = os.path.join(path, 'reporting.html')
        with open(reporting, 'w') as f:
            f.write(
                WENG.avwa.template(
                    'reporting',
                    chain_name=name,
                    date_upload=chain[0][0][1],
                    date=datetime.datetime.now().strftime("%d/%m/%Y %H:%M"),
                    errors=cm.errors,
                    warnings=cm.warnings,
                    libmemsym=cm.libmemsym_values,
                    libmemsym_used=cm.libmemsym_used,
                    schema=schema,
                    schema_groupes=schema_groupes,
                    csv=os.path.basename(csv)
                    ))
            
        files_list.append(reporting)


        os.remove(myfile)

        # Create zip file for storing result
        with zipfile.ZipFile(os.path.join(path, "Analyse_{0}.zip".format(name)), 'w') as mzip:
            for fn in files_list:
                sfn = os.path.join(name, os.path.split(fn)[-1])
                mzip.write(filename=fn, arcname=sfn)

        # # Create tar file for storing result
        # tar = tarfile.open(os.path.join(path, "Analyse_{0}.tar".format(name)), 'w')
        # tar.add(path, arcname='/')
        # tar.close()

        logger.removeHandler(fh)
        fh.close()


        # Delete files after serving them...
        mytimer = threading.Timer(5.0, delete_later, [path], {})
        mytimer.start()

        
        return WENG.avwa.static_file("Analyse_{0}.zip".format(name), root=path, download=True)
        # return WENG.avwa.static_file("Analyse_{0}.tar".format(name), root=path, download=True)

    return WENG.avwa.redirect('/')


@log
def delete_later(path):
    """
    Delete files in path after the timeout
    
    Arguments:
    - `path`: filename to delete
    """
    print('Removing directory : {0}'.format(path))
    shutil.rmtree(path, ignore_errors = True)
    return


@log
def drop_privileges(run_as_uid, run_as_gid):
    """
    Drop privileges to username run_as_uid / group name run_as_gid

    Arguments:
    - `run_as_uid`: username or list of username to run as
    - `run_as_gid`: groupname or list of groupname to run as
    """
    nlogger = logging.getLogger("WS_CA")

    uid = gid = None
    suid = run_as_uid
    sgid = run_as_gid

    if isinstance(run_as_uid, str):
        uid = pwd.getpwnam(run_as_uid).pw_uid
    elif isinstance(run_as_uid, list) or isinstance(run_as_uid, tuple):
        for ruid in run_as_uid:
            try:
                uid = pwd.getpwnam(ruid).pw_uid
                suid = ruid
            except KeyError:
                nlogger.debug('User {0} not found'.format(ruid))
            else:
                break
        else:
            nlogger.error('Could not find any user id ({0})'.format(run_as_uid))
            raise KeyError('User ({0}) not found, could not drop privileges'.format(run_as_uid))


    if isinstance(run_as_gid, str):
        gid = grp.getgrnam(run_as_gid).gr_gid
    elif isinstance(run_as_gid, list) or isinstance(run_as_gid, tuple):
        for rgid in run_as_gid:
            try:
                gid = grp.getgrnam(rgid).gr_gid
                sgid = rgid
            except KeyError:
                nlogger.debug('Group {0} not found'.format(rgid))
            else:
                break
        else:
            nlogger.error('Could not find any group id ({0})'.format(run_as_gid))
            raise KeyError('Group ({0}) not found, could not drop privileges'.format(run_as_gid))


    nlogger.info("Running as {0}/{1}".format(os.getuid(), os.getgid()))

    if os.getuid()==uid and os.getgid()==gid:
        nlogger.info("Already running as {0}/{1}".format(suid, sgid))
        return
    elif os.getuid()==0:
        nlogger.info("Dropping privileges to [{0}:{2}/{1}:{3}]".format(suid, sgid, uid, gid))
        try:
            os.setgid(gid)
        except OSError:
            nlogger.error('Could not set effective group id')

        try:
            os.setgroups([])
        except OSError:
            nlogger.error('Could not drop groups')
            

        try:
            os.setuid(uid)
        except OSError:
            nlogger.error('Could not set effective group id')
    else:
        nlogger.error(
            "Can't drop privileges to UID/GID [{0}/{1}]. Running as [{2}/{3}]".format(uid,
                                                                                      gid,
                                                                                      os.getuid(),
                                                                                      os.getgid()))
    return


@clize.clize(
    alias = {
        'port': ('p',),
        'host': ('H',),
        'uid': ('u',),
        'gid': ('g',),
        },
    coerce={'port': int, 'host': str, 'uid': str, 'gid':str}
    )
def __main__(port=8080, host='0.0.0.0', uid='nobody', gid=['nobody', 'nogroup'], debug=False):
    """
    web_service.py

    port: listening port

    host: listening IP address

    uid: username to run as

    gid: group name to run as

    debug: debug flag

    Extract control-M schedtable.xml from tar file if [controlm-file] is a tar file or
    directly parse [controlm-file] to create output suitable for graphviz (with dot).

    Written by : Alexandre Norman <norman at xael.org>
    """

    if platform.uname()[0] in ['Linux']: 
        locale.setlocale(locale.LC_ALL,'fr_FR.UTF-8')


    # logging service WS_CA
    logger = logging.getLogger("WS_CA")
    fh = logging.StreamHandler()
    if debug:        
        logger.setLevel(logging.DEBUG)
        fh.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        fh.setLevel(logging.INFO)
        
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    


    ulogger = logging.getLogger("CM_Usage")
    ulogger.setLevel(logging.INFO)
    ufh = logging.FileHandler(os.path.join('uploads','usage.log'), mode='a', encoding='utf-8', delay=False)
    uformatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s\r\n')
    ufh.setFormatter(uformatter)
    ulogger.addHandler(ufh)

    
    if sys.platform != 'win32':
        drop_privileges(run_as_uid=uid, run_as_gid=gid)
    else:
        logger.info('Win32 OS, not dropping privileges')

    WebEngine(port=port, host=host, debug=debug)
    return


# MAIN -------------------
if __name__ == '__main__':
    clize.run(__main__)

