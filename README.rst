=================
Analyse Control-M
=================

This is a python script which allows to extract informations from scheduling
tables created with **BMC Control-M**.  It also include a web-interface for easy
use. You can see it online at controlm.xael.org_.

It extract control-M ``schedtable.xml`` from tar file if ``[controlm-file]`` is a
tar file or directly parse ``[controlm-file]`` to create output suitable
for graphviz (with dot).

.. _controlm.xael.org: http://controlm.xael.org/
