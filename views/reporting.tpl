<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="/static/style.css" type="text/css" media="screen" />
  <link rel="icon" type="image/png" href="/static/favicon.png" />
  <link rel="icon" type="image/ico" href="/static/favicon.ico" />
<style media="screen" type="text/css">
body {}
body { font-size: 0.8125em; font-family: "Luxi sans", "Lucida Grande", Lucida, "Lucida Sans Unicode", sans-serif; color: #333; margin: 10px 20px 10px 20px; min-width:550px;}

div.cleaner {
	clear: both;
    height: 33px;
}

div.header { 
    color: #006; 
    background: #eef; 
    padding: 5px 10px 5px 10px; 
    /*border-radius: 10px; */
    position: fixed;
    top: 0px;
    left: 0px;
    width: 100%;
}

h1 { color: #006; background: #eef; padding: 5px 10px 5px 10px; border-radius: 10px;}
h2 { color: #00a; }


a { }
a:link { color: #009; }
a:visited { color: #009; }
a:hover { color: #b00; }
a:active { color: #f00; }

.hidden {
    display: none;
}

label {
    display: block;
    float: left;
    width: 180px;
    text-align: right;
    margin-right: 15px;
}

input, textarea, select, input.submit, #tags_affiches {
    display: block;
    width: 250px;
    margin-bottom: 5px;
}


#submit {
    width: 250px;
    margin-left: 195px;
}

fieldset {
    padding: 20px 60px 20px 20px;
    /*width: 100px;*/
    border-radius: 10px;
}

input[type=text], input[type=date], input[type=password], textarea, select    {
    border: 1px solid #999;
    background: #fff;
}


input:focus, textarea:focus, select:focus {
    border: 1px solid #C90;
    box-shadow: 0px 0px 8px rgba(250, 80, 0, 0.3);
}

div.error, .obligatoire {
    background: #fee;
}

div.error {
    margin-top: 10px;
    margin-bottom: 10px;
    padding: 10px 10px 10px 10px;
    font-family: "Lucida Console", Monaco, monospace;
}
.footer {
	border-top: 1px solid #dbdbdb;
	padding-top: 10px;
	margin-top: 20px;
	text-align: right;

	color: #006; 
	background: #eef; 
	border-radius: 10px;
	padding: 5px 10px 5px 10px; 
}

td.jobname {
    font-family: Courier, monospace;
}

table{margin-bottom:18px;padding:0;border-collapse:collapse;}table th,table td{padding:10px 10px 9px;line-height:18px;text-align:left;}
table th{padding-top:9px;font-weight:bold;vertical-align:middle;border:1px solid #ddd;background-color:#f5f5f5}
table tfoot{padding-top:9px;font-weight:bold;vertical-align:middle;border:1px solid #ddd;background-color:#f5f5f5}
table td{vertical-align:top;border:1px solid #ddd;}
table tbody th{border-top:1px solid #ddd;vertical-align:top;}

div.schemas {
    float: left;
    margin: 30px;
}

.clearBoth { clear:both; }

.t_mess { width: 600px; }
.t_risk { width: 300px; }
</style>

<script type="text/javascript">
  function showHide(divId)
  {
  if (document.getElementById(divId).style.display=="none")
  {
  document.getElementById(divId).style.display="inline";
  }
  else
  {
  document.getElementById(divId).style.display="none";
  }
  }
  
  function show(divId)
  {
  document.getElementById(divId).style.display="block";
  }
  function hide(divId)
  {
  document.getElementById(divId).style.display="none";
  }
</script>

</head>
<body>
<h1>Analyseur de cha&icirc;nes Control-M</h1>

<h2>Sommaire</h2>
<ul>
  <li><a href="#id">Identification</a></li>
  <li><a href="#errors">Erreurs relev&eacute;es</a></li>
  <li><a href="#warnings">Avertissements</a></li>
%if len(libmemsym)>0:
  <li><a href="#libmemsym">Libmemsym</a></li>
%end
%if len(schema_groupes) > 0 or len(schema) > 0:
  <li><a href="#schemas">Sch&eacute;mas</a></li>
%end
</ul>

<hr>

<a id="id"><h2>Identification</h2></a>
<table>
<tr><td>Cha&icirc;ne</td><td><tt>{{chain_name}}</tt></td></tr>
<tr><td>Date d'analyse</td><td>{{date}}</td></tr>
<tr><td>Date d'upload</td><td>{{date_upload}}</td></tr>
</table>


<a id="errors"><h2>Erreurs relev&eacute;es</h2></a>
%if len(errors)>0:
<table>
<tr><th>Nom du job</th><th class="t_mess">Message</th><th class="t_risk">Risque</th></tr>
%for e in errors:
<tr><td class="jobname">{{e['job']}}</td><td class="t_mess">{{!e['reason']}}</td><td class="t_risk">{{!e['risk']}}</td></tr>
%end
</table>
%else:
<p>Pas d'erreur relev&eacute;e dans cette cha&icirc;ne.
%end

<a id="warnings"><h2>Avertissements</h2></a>
%if len(warnings)>0:
<table>
<tr><th>Nom du job</th><th class="t_mess">Message</th><th class="t_risk">Risque</th></tr>
%for e in warnings:
<tr><td class="jobname">{{e['job']}}</td><td class="t_mess">{{!e['reason']}}</td><td class="t_risk">{{!e['risk']}}</td></tr>
%end
</table>
%else:
<p>Pas d'avertissement pour cette cha&icirc;ne.
%end


%if len(libmemsym)>0:
<a id="libmemsym"><h2>Libmemsym</h2></a>
<table>
  <tr><th>Variable</th><th>Valeur</th><th>Nombre d'utilisations</th></tr>
%for v in libmemsym:
%if v in libmemsym_used:
%nb=libmemsym_used[v]
%else:
%nb=0
%end
  <tr><td>{{v}}</td><td>{{libmemsym[v]}}</td><td>{{nb}}</td></tr>
%end
</table>

%end


%if len(schema_groupes) > 0 or len(schema) > 0:
<a id="schemas"><h2>Sch&eacute;mas</h2></a>
%end

%if len(schema) > 0:
<h3>Cha&icirc;ne globale</h3>
<a href="{{schema}}"><img src="{{schema}}" height="300"></a>
%end

%if len(schema_groupes) > 0:
<h3>D&eacute;tail des groupes</h3>
%for g in schema_groupes:
%nom=g.split('_')[-1].split('.')[0]
<div class="schemas">{{nom}}<br>
<a href="{{g}}"><img src="{{g}}" height="200"></a>
</div>
%end
%end

<br class="clearBoth" />
<div class="footer">&nbsp;</div>
</body>
</html>
