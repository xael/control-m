<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="/static/style.css" type="text/css" media="screen" />
  <link rel="icon" type="image/png" href="/static/favicon.png" />
  <link rel="icon" type="image/ico" href="/static/favicon.ico" />
<style media="screen" type="text/css">
body {}
body { font-size: 0.8125em; font-family: "Luxi sans", "Lucida Grande", Lucida, "Lucida Sans Unicode", sans-serif; color: #333; margin: 10px 20px 10px 20px; min-width:550px;}

div.cleaner {
	clear: both;
    height: 33px;
}

div.header { 
    color: #006; 
    background: #eef; 
    padding: 5px 10px 5px 10px; 
    /*border-radius: 10px; */
    position: fixed;
    top: 0px;
    left: 0px;
    width: 100%;
}

h1 { color: #006; background: #eef; padding: 5px 10px 5px 10px; border-radius: 10px;}
h2 { color: #00a; }


a { }
a:link { color: #009; }
a:visited { color: #009; }
a:hover { color: #b00; }
a:active { color: #f00; }

.hidden {
    display: none;
}

label {
    display: block;
    float: left;
    text-align: right;
    margin-right: 15px;
}

input, textarea, select, input.submit, #tags_affiches {
    display: block;
    width: 250px;
    margin-bottom: 5px;
}



#submit {
    width: 250px;
    margin-left: 195px;
}

fieldset {
    padding: 20px 60px 20px 20px;
    /*width: 100px;*/
    width: 490px;
    border-radius: 10px;
}

input[type=text], input[type=date], input[type=password], textarea, select    {
    border: 1px solid #999;
    background: #fff;
}

input[type=checkbox].chkbx {
    display: inline;
    width: 10px;
}



label.chkbx {
    display: inline;
    float: none;
    width: 180px;
    text-align: right;
    margin-right: 15px;
}


input:focus, textarea:focus, select:focus {
    border: 1px solid #C90;
    box-shadow: 0px 0px 8px rgba(250, 80, 0, 0.3);
}

div.error, .obligatoire {
    background: #fee;
}

div.error {
    margin-top: 10px;
    margin-bottom: 10px;
    padding: 10px 10px 10px 10px;
    font-family: "Lucida Console", Monaco, monospace;
}
.footer {
	border-top: 1px solid #dbdbdb;
	padding-top: 10px;
	margin-top: 20px;
	text-align: right;

	color: #006; 
	background: #eef; 
	border-radius: 10px;
	padding: 5px 10px 5px 10px; 
}
</style>

<script type="text/javascript">
  function showHide(divId)
  {
  if (document.getElementById(divId).style.display=="none")
  {
  document.getElementById(divId).style.display="inline";
  }
  else
  {
  document.getElementById(divId).style.display="none";
  }
  }
  
  function show(divId)
  {
  document.getElementById(divId).style.display="block";
  }
  function hide(divId)
  {
  document.getElementById(divId).style.display="none";
  }
</script>

</head>
<body>
<form action="/" enctype="multipart/form-data" method="post" onsubmit="show('legende');hide('erreurs');return(true);">
<h1>Analyseur de chaînes Control-M</h1>

<h2>Utilisation</h2>
<ol>
  <li>Télécharger un export de chaîne Control-M</li>
  <li>Après calcul, un fichier <tt>CHAINE.tar</tt> est retourné. Il comprend :
    <ul>
      <li><tt>CHAINE.csv</tt> qui contient l'extraction des jobs consultable dans un tableur</li>
      <li><tt>CHAINE.png</tt> qui est le schéma global de la chaîne</li>
      <li>plusieurs fichiers <tt>CHAINE_GROUPE.png</tt>, schéma détaillé de chaque groupe</li>
      <li>un fichier <tt>errors_and_warnings.txt</tt></li>
    </ul>
  </li>
</ol>



<h2>Télécharger un fichier</h2>
<fieldset>
<legend>Fichier a analyser :</legend>
<label for="datafile">Fichier (.tar ou schedtable.xml)</label>
<input type="file" name="datafile" class="obligatoire">

<br clear="all">
<ul>
<li><input class="chkbx" type="checkbox" name="gfx_global" checked><label for="gfx_global" class="chkbx">Image de la chaîne complète</label></li>
<li><input class="chkbx" type="checkbox" name="gfx_detaille" checked><label for="gfx_detaille" class="chkbx">Images de chaque groupe de jobs</label></li>
<li><input class="chkbx" type="checkbox" name="graphviz"><label for="graphviz" class="chkbx">Fichiers Graphviz</label></li>
<li><input class="chkbx" type="checkbox" name="descriptions" checked><label for="descriptions" class="chkbx">Inclure les descriptions des jobs dans les sch&eacute;mas</label></li>
</ul>


<label for="submit">&nbsp;</label>
<input type="submit" id="submit" name="submit">
</fieldset>
</form>

%if error:
<div id="erreurs">
<h2>Erreur lors du traitement du fichier <tt>{{filename}}</tt></h2>
{{!error}}
</div>
<div id="legende" class="hidden">
%else:
<div id="legende">
%end
<h2>Légende des schémas</h2>
<h3>Réprésentation des jobs</h3>
<ul>
  <li>Les jobs normaux sont des carrés sur fond blanc</li>
  <li>Les jobs en dummy sont des carrés sur fond jaune</li>
  <li>Les jobs qui nécessitent une confirmation sont des carrés sur fond rose</li>
  <li>Les jobs en vert sont des jobs cycliques lancés à intervale régulier</li>
</ul>

Jusqu'à quatre lignes peuvent être présentes dans un bloc décrivant un job :
<ul>
  <li>le nom du job</li>
  <li>les jours (et date du jour d'exécution)</li>
  <li>l'horaire d'exécution</li>
  <li>le NodeID</li>
</ul>

<h3>Réprésentation des liaisons entre les jobs</h3>
<ul>
  <li>Les liaisons simples (ET logique) sont des flèches noires</li>
  <li>Les liaisons OU logique sont des flèches bleues annotées <tt>OR</tt> </li>
  <li>Les liaisons basées sur un KO sont annotées d'un <tt>ko</tt> rouge</li>
  <li>Les liaisons supprimant une condition sont des flèches rouges annotées <tt>DEL</tt></li>
</ul>
</div>


<div class="footer">
{{!footer}}
</div>
</body>
</html>
